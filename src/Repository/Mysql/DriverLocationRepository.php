<?php

/**
 * NOTE: This is a temporary implementation of the nearest driver algorithm. This will be replaced by a common API in the future
 */

namespace Pickme\DataAccess\Repository\Mysql;


class DriverLocationRepository
{
    public function getNearestDrivers($locationData)
    {
        $latitude = $locationData['lat'];
        $longitude = $locationData['lng'];
        $taxi_model = $locationData['model'];
        $radius = $locationData['radius'];
        $brand_model_id = $locationData['brand'];

        $taxi_condition = "";

        if(($taxi_model != 'All') && ($taxi_model != null))
        {
            $taxi_condition.= " AND taxi_model='".$taxi_model."' ";
        }

        $distance_query="";

        if($radius)
        {
            $distance_query = "HAVING updatetime_difference <= '15' ";
        }

        $get_xy_values = $this->getMinMaxCoords($latitude,$longitude,$radius);

        $x1 = $get_xy_values[0][0];
        $x2 = $get_xy_values[0][1];
        $y1 = $get_xy_values[0][2];
        $y2 = $get_xy_values[0][3];

        $brand_model_select = "";
        $brand_model_join = "";
        $brand_model_order_by = "";

        if($brand_model_id)
        {
            $brand_model_select = ",taxi_information.brand_model_id, vehicle_model.name AS brand_name";

            $brand_model_join="JOIN taxi_information ON tx.taxi_id = taxi_information.taxi_id
            AND taxi_information.brand_model_id='".$brand_model_id."'
            LEFT JOIN vehicle_model ON taxi_information.brand_model_id=vehicle_model.id";

            $brand_model_order_by = "taxi_information.brand_model_id DESC,";
        }

        $query="SELECT
                pp.name as name,
                driver_id,
                tdm.mapping_companyid AS company_id,
                driver.latitude,
                driver.longitude,
                tdm.mapping_taxiid AS taxi_id,
                tx.taxi_speed,
                ROUND((POW((longitude-($longitude)),2) + POW((latitude-($latitude)),2)),5) as distance,
                (select (Time_to_sec(Timediff(NOW(), driver.update_date)))) AS updatetime_difference
                $brand_model_select
            FROM driver
            JOIN taxi_driver_mapping AS tdm ON tdm.mapping_driverid=driver.driver_id 
            JOIN taxi AS tx ON tdm.mapping_taxiid=tx.taxi_id 
            JOIN people AS pp ON tdm.mapping_driverid=pp.id 
            $brand_model_join
            where
            driver.status='F' 
            AND latitude between $x1 and $x2
            and longitude between $y1 and $y2
            AND driver.shift_status='IN' 
            AND tdm.mapping_status='A' 
            AND tx.taxi_status='A'
            $taxi_condition
            AND pp.status='A'
            AND pp.login_status='S'
            $distance_query
            ORDER BY distance asc
            limit 5";

        return app('db')->select($query);
    }


    public function getDestinationPoint($alat, $alon, $distance, $bearing)
    {
        $pi=3.14159265358979;
        $alatRad=$alat*$pi/180;
        $alonRad=$alon*$pi/180;
        $bearing=$bearing*$pi/180;
        $alatRadSin=sin($alatRad);
        $alatRadCos=cos($alatRad);
        // Ratio of distance to earth's radius
        $angularDistance=$distance/6370.997;
        $angDistSin=sin($angularDistance);
        $angDistCos=cos($angularDistance);
        $xlatRad = asin( $alatRadSin*$angDistCos +
            $alatRadCos*$angDistSin*cos($bearing) );
        $xlonRad = $alonRad + atan2(
                sin($bearing)*$angDistSin*$alatRadCos,
                $angDistCos-$alatRadSin*sin($xlatRad));
        // Return latitude and longitude as two element array in degrees
        $xlat=$xlatRad*180/$pi;
        $xlon=$xlonRad*180/$pi;
        if($xlat>90)$xlat=90;
        if($xlat<-90)$xlat=-90;
        while($xlat>180)$xlat-=360;
        while($xlat<=-180)$xlat+=360;
        while($xlon>180)$xlon-=360;
        while($xlon<=-180)$xlon+=360;
        return array($xlat,$xlon);
    }


    // Distance is in km, lat and lon are in degrees
    public function getSquareAroundPoint($lat,$lon,$distance)
    {
        return array(
            $this->getDestinationPoint($lat,$lon,$distance,0), // Get north point
            $this->getDestinationPoint($lat,$lon,$distance,90), // Get east point
            $this->getDestinationPoint($lat,$lon,$distance,180), // Get south point
            $this->getDestinationPoint($lat,$lon,$distance,270) // Get west point
        );
    }


    // Returns array containing an array with min lat, max lat, min lon, max lon
    // If the square defining these points crosses the 180-degree meridian, two
    // such arrays are returned.  Otherwise, one such array (within another array)
    // is returned.
    public function getMinMaxCoords($lat,$lon,$distance)
    {
        $s=$this->getSquareAroundPoint($lat,$lon,$distance);

        if($s[3][1] > $s[1][1]) // if west longitude is greater than south longitude
        {
            // Crossed the 180-degree meridian
            return array(
                array($s[2][0],$s[0][0],$s[3][1],180),
                array($s[2][0],$s[0][0],-180,$s[1][1])
            );
        }
        else
        {
            // Didn't cross the 180-degree meridian (usual case)
            return array(
                array($s[2][0],$s[0][0],$s[3][1],$s[1][1])
            );
        }
    }

}