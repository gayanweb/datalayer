<?php

namespace Pickme\DataAccess\Repository\Mysql;

use Carbon\Carbon;
use Pickme\DataAccess\Repository\Mysql\Model\Company;
use Pickme\DataAccess\Repository\Mysql\Model\DriverRequestCurrent as DriverRequest;
use Pickme\DataAccess\Repository\Mysql\Model\ModifiedFare;
use Pickme\DataAccess\Repository\Mysql\Model\Package;
use Pickme\DataAccess\Repository\Mysql\Model\Passenger;
use Pickme\DataAccess\Repository\Mysql\Model\PassengerLogCurrent as PassengerLog;
use Pickme\DataAccess\Repository\Mysql\Model\People;
use Pickme\DataAccess\Repository\Mysql\Model\Transaction;
use Pickme\DataAccess\Repository\Mysql\Model\MotorModel;

use Lib\RequestHandler\RequestMapper;

use Illuminate\Database\Query\Builder;

class BookingRepository
{
    /**
     * @var RequestMapper
     */
    private $requestMapper;

    /**
     * @var PassengerLog
     */
    private $passengerLog;

    /**
     * @var DriverRequest
     */
    private $driverRequest;


    /**
     * BookingRepository constructor.
     *
     * @param RequestMapper $requestMapper
     * @param PassengerLog $passengerLog
     * @param DriverRequest $driverRequest
     */
    public function __construct(RequestMapper $requestMapper,
                                PassengerLog $passengerLog,
                                DriverRequest $driverRequest
    )
    {
        $this->requestMapper = $requestMapper;
        $this->passengerLog = $passengerLog;
        $this->driverRequest = $driverRequest;
    }


    /**
     * Get the list of bookings
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getList()
    {
        // set mappings to filters ___

        $this->requestMapper->setMappings([

            // dynamic filters
            "passenger_phone" => [
                'field' => Passenger::TABLE . '.phone'
            ],
            "booked_from" => [
                'field' => PassengerLog::TABLE . '.booking_from'
            ],
            "trip_status" => [
                'field' => PassengerLog::TABLE . '.travel_status'
            ],
            "company_id" => [
                'field' => PassengerLog::TABLE . '.company_id'
            ],
            "pickup_date" => [
                'field' => PassengerLog::TABLE . '.pickup_time',
                'callBack' => function($value) {
                    $date = Carbon::parse($value);
                    return $date->format('Y-m-d');
                }
            ],

            // custom filters
            "booked_by" => [
                'autoAppend' => false
            ],

        ]);


        /* @var Builder $builder */
        $builder = app('db')->table(PassengerLog::TABLE);

        $builder->addSelect([
                        // trip details
                        PassengerLog::TABLE . '.passengers_log_id AS trip_id',
                        PassengerLog::TABLE . '.travel_status',
                        PassengerLog::TABLE . '.bookby AS booked_by',
                        PassengerLog::TABLE . '.no_passengers',
                        PassengerLog::TABLE . '.dispatch_time',
                        PassengerLog::TABLE . '.pickup_time',
                        PassengerLog::TABLE . '.driver_reply',
                        PassengerLog::TABLE . '.notes_driver',
                        DriverRequest::TABLE . '.total_drivers',
                        PassengerLog::TABLE . '.approx_distance',
                        Transaction::TABLE . '.distance',

                        // locations
                        PassengerLog::TABLE . '.current_location',
                        PassengerLog::TABLE . '.pickup_latitude',
                        PassengerLog::TABLE . '.pickup_longitude',
                        PassengerLog::TABLE . '.drop_location',
                        PassengerLog::TABLE . '.drop_latitude',
                        PassengerLog::TABLE . '.drop_longitude',

                        // passenger
                        Passenger::TABLE . '.id AS passenger_id',
                        Passenger::TABLE . '.name AS passenger_name',
                        Passenger::TABLE . '.phone AS passenger_phone',

                        // driver
                        PassengerLog::TABLE . '.driver_id',
                        People::TABLE . '.name AS driver_name',
                        People::TABLE . '.phone AS driver_phone',
                        People::TABLE . '.reachable_mobile AS driver_reachable_phone',

                        // taxi
                        PassengerLog::TABLE . '.taxi_id',
                        PassengerLog::TABLE . '.taxi_modelid',

                        // promo
                        PassengerLog::TABLE . '.promocode',

                        // company
                        PassengerLog::TABLE . '.company_id',
                        Company::TABLE . '.company_name',

                        // costing
                        PassengerLog::TABLE . '.approx_fare',
                        Transaction::TABLE . '.fare',

                        // flags
                        ModifiedFare::TABLE . '.booking_from',
        ]);


        $builder->leftJoin(Passenger::TABLE, Passenger::TABLE . '.id', '=', PassengerLog::TABLE . '.passengers_id')
                ->leftJoin(Company::TABLE, Company::TABLE . '.cid', '=', PassengerLog::TABLE . '.company_id')
                ->leftJoin(People::TABLE, People::TABLE . '.id', '=', PassengerLog::TABLE . '.driver_id')
                ->leftJoin(DriverRequest::TABLE, DriverRequest::TABLE . '.trip_id', '=', PassengerLog::TABLE . '.passengers_log_id')
                ->leftJoin(Transaction::TABLE, Transaction::TABLE . '.passengers_log_id', '=', PassengerLog::TABLE . '.passengers_log_id')
                ->leftJoin(ModifiedFare::TABLE, ModifiedFare::TABLE . '.trip_id', '=', PassengerLog::TABLE . '.passengers_log_id');


        // apply unique filters ___

        // get only trips with pickup time between 2 hours before now and not exceeding 15 days from now
        $builder->where(PassengerLog::TABLE . '.pickup_time', '>', app('db')->raw("TIMESTAMPADD(MINUTE, -120, NOW())"))
                ->where(PassengerLog::TABLE . '.pickup_time', '<', app('db')->raw("TIMESTAMPADD(DAY, 15, NOW())"));


        // apply conditional filters ___
        $bookedBy = $this->requestMapper->getRawFilter('booked_by');

        if(!empty($bookedBy))
        {
            // bookings done by dispatcher
            if ($bookedBy['value'] === PassengerLog::BOOK_BY_OPERATOR)
            {
                $builder->where(function ($whereGroup)
                {
                    $whereGroup->where(function ($whereSubGroup)
                                        {
                                            $whereSubGroup->where(PassengerLog::TABLE . '.bookby', '=', PassengerLog::BOOK_BY_OPERATOR)
                                                          ->where(PassengerLog::TABLE . '.now_after', '!=', PassengerLog::BOOK_LATER);
                                        })
                                ->orWhere(function ($whereSubGroup)
                                        {
                                            $whereSubGroup->where(PassengerLog::TABLE . '.booking_from', '=', PassengerLog::BOOK_FROM_DEVICE)
                                                          ->where(ModifiedFare::TABLE . '.booking_from', '=', ModifiedFare::BOOK_FROM_PASSENGER)
                                                          ->where(PassengerLog::TABLE . '.now_after', '=', PassengerLog::BOOK_LATER);
                                        });
                });
            }

            // bookings done by passenger app
            if ($bookedBy['value'] === PassengerLog::BOOK_BY_PASSENGER)
            {
                $builder->where(PassengerLog::TABLE . '.now_after', '=', PassengerLog::BOOK_LATER);
                $builder->whereNull(ModifiedFare::TABLE . '.trip_id');
            }
        }


        // apply dynamic filters ___
        $this->requestMapper->applyFilters($builder);


        $builder->orderBy(PassengerLog::TABLE . '.pickup_time', 'asc');


        return $builder->simplePaginate($this->requestMapper->getPaging()['perPage'])->items();

    }


    /**
     * Get details of a booking by its id
     *
     * @param $id
     * @return mixed|static
     */
    public function getById($id)
    {
        /* @var Builder $builder */
        $builder = app('db')->table(PassengerLog::TABLE);

        $builder->addSelect([
            // trip details
            PassengerLog::TABLE . '.passengers_log_id AS booking_id',
            PassengerLog::TABLE . '.travel_status',
            PassengerLog::TABLE . '.bookby AS booked_by',
            PassengerLog::TABLE . '.no_passengers',
            PassengerLog::TABLE . '.dispatch_time',
            PassengerLog::TABLE . '.pickup_time',
            PassengerLog::TABLE . '.driver_reply',
            PassengerLog::TABLE . '.notes_driver',
            PassengerLog::TABLE . '.approx_distance',
            PassengerLog::TABLE . '.promocode',

            // vehicle
            PassengerLog::TABLE . '.taxi_modelid AS vehicle_type',
            MotorModel::TABLE . '.model_name',

            // locations
            PassengerLog::TABLE . '.current_location',
            PassengerLog::TABLE . '.pickup_latitude',
            PassengerLog::TABLE . '.pickup_longitude',
            PassengerLog::TABLE . '.drop_location',
            PassengerLog::TABLE . '.drop_latitude',
            PassengerLog::TABLE . '.drop_longitude',

            // passenger
            Passenger::TABLE . '.id AS passenger_id',
            Passenger::TABLE . '.name AS passenger_name',
            Passenger::TABLE . '.phone AS passenger_phone',
            Passenger::TABLE . '.email AS passenger_email',

            // driver
            PassengerLog::TABLE . '.driver_id',
            People::TABLE . '.name AS driver_name',
            People::TABLE . '.phone AS driver_phone',
            People::TABLE . '.reachable_mobile AS driver_reachable_phone',

            // taxi
            PassengerLog::TABLE . '.taxi_id',
            PassengerLog::TABLE . '.taxi_modelid',

            // promo
            PassengerLog::TABLE . '.promocode',

            // company
            PassengerLog::TABLE . '.company_id',

            // flags
            ModifiedFare::TABLE . '.booking_from',

            // package
            Package::TABLE . '.package_name',
            ModifiedFare::TABLE . '.package_type',
            ModifiedFare::TABLE . '.package_id',
            ModifiedFare::TABLE . '.min_km',
            ModifiedFare::TABLE . '.above_km',
            ModifiedFare::TABLE . '.waiting_time',
            ModifiedFare::TABLE . '.free_waiting_time',
            ModifiedFare::TABLE . '.ride_hours',
            ModifiedFare::TABLE . '.min_fare',
            ModifiedFare::TABLE . '.extra_ride_fare',
            ModifiedFare::TABLE . '.night_fare',
            ModifiedFare::TABLE . '.driver_bata',
        ]);


        $builder->join(Passenger::TABLE, Passenger::TABLE . '.id', '=', PassengerLog::TABLE . '.passengers_id')
                ->leftJoin(People::TABLE, People::TABLE . '.id', '=', PassengerLog::TABLE . '.driver_id')
                ->leftJoin(ModifiedFare::TABLE, ModifiedFare::TABLE . '.trip_id', '=', PassengerLog::TABLE . '.passengers_log_id')
                ->leftJoin(Package::TABLE, Package::TABLE . '.id', '=', ModifiedFare::TABLE . '.package_id')
                ->join(MotorModel::TABLE, MotorModel::TABLE . '.model_id', '=', PassengerLog::TABLE . '.taxi_modelid');


        $builder->where(PassengerLog::TABLE . '.passengers_log_id', '=', $id);


        return $builder->first();
    }


    /**
     * Create a booking
     *
     * @param $booking
     * @return static
     */
    public function create($booking)
    {
        return $this->passengerLog->create($booking);
    }


    /**
     * Update a booking
     *
     * @param $booking
     * @return static
     */
    public function update($booking)
    {
        return $booking->save();
    }


    /**
     * Cancel a booking
     *
     * @param $booking
     * @param $message
     * @return mixed
     */
    public function cancel($booking, $message)
    {
        $booking->travel_status = PassengerLog::TRIP_STATUS_DISPATCHER_CANCEL;
        $booking->comments = $message;

        return $booking->save();
    }


    /**
     * Get a booking by a single field
     *
     * @param $field
     * @param $value
     * @param array $returnFields
     * @return mixed
     */
    public function getBy($field, $value, $returnFields = ['*'])
    {
        return $this->passengerLog->where($field, '=', $value)->first($returnFields);
    }


    /**
     * Create a driver request
     *
     * @param $driverRequest
     * @return static
     */
    public function createDriverRequest($driverRequest)
    {
        return $this->driverRequest->create($driverRequest);
    }
}