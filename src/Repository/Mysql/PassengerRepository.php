<?php

namespace Pickme\DataAccess\Repository\Mysql;

use Pickme\DataAccess\Repository\Mysql\Model\Passenger;

use Lib\RequestHandler\RequestMapper;

use Illuminate\Database\Query\Builder;

class PassengerRepository
{
    /**
     * @var Passenger
     */
    private $passenger;

    /**
     * @var RequestMapper
     */
    private $requestMapper;


    /**
     * PassengerRepository constructor.
     *
     * @param RequestMapper $requestMapper
     * @param Passenger $passenger
     */
    public function __construct(RequestMapper $requestMapper,
                                Passenger $passenger)
    {
        $this->passenger = $passenger;
        $this->requestMapper = $requestMapper;
    }


    /**
     * Get a list of passengers
     *
     * @return array|static[]
     */
    public function getList()
    {
        // set mappings to filters and fields
        $this->requestMapper->setMappings([

            // dynamic filters
            "passenger_phone" => [
                'field' => Passenger::TABLE . '.phone'
            ],
            "passenger_id" => [
                'field' => Passenger::TABLE . '.id'
            ],
            "passenger_name" => [
                'field' => Passenger::TABLE . '.name'
            ],
            "passenger_email" => [
                'field' => Passenger::TABLE . '.email'
            ],
            "passenger_created" => [
                'field' => Passenger::TABLE . '.created_by'
            ],
        ]);


        /* @var Builder $builder */
        $builder = app('db')->table(Passenger::TABLE);

        // apply unique filters
        // get only active passengers
        $builder->where(Passenger::TABLE . '.user_status', '=', Passenger::ACTIVE);

        // apply dynamic filters
        $this->requestMapper->applyFilters($builder);

        // apply dynamic fields
        $this->requestMapper->applyFields($builder);

        // limit
        $builder->limit(15);


        return $builder->get();

    }


    /**
     * Get a passenger by a single field
     *
     * @param $field
     * @param $value
     * @param array $returnFields
     * @return mixed
     */
    public function getBy($field, $value, $returnFields = ['*'])
    {
        return $this->passenger->where($field, '=', $value)->first($returnFields);
    }

    /**
     * Crete a passenger
     *
     * @param $passenger
     * @return static
     */
    public function create($passenger)
    {
        return $this->passenger->create($passenger);
    }


    /**
     * Get passenger status
     *
     * @param $passengerId
     * @return mixed
     */
    public function getPassengerStatus($passengerId)
    {
        return app('db')->table(Passenger::TABLE)
            ->select('id', 'user_status')
            ->where('id', $passengerId)
            ->value('user_status');
    }

}