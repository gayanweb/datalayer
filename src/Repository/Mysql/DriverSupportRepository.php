<?php

namespace Pickme\DataAccess\Repository\Mysql;

use Pickme\DataAccess\Repository\Mysql\Model\Driver;
use Pickme\DataAccess\Repository\Mysql\Model\DriverContactInfo;
use Pickme\DataAccess\Repository\Mysql\Model\DriverSupportReasonLog;
use Pickme\DataAccess\Repository\Mysql\Model\MotorModel;
use Pickme\DataAccess\Repository\Mysql\Model\Passenger;
use Pickme\DataAccess\Repository\Mysql\Model\PassengerLogCurrent as PassengerLog;
use Pickme\DataAccess\Repository\Mysql\Model\Taxi;
use Pickme\DataAccess\Repository\Mysql\Model\TaxiDriverMap;

use Lib\RequestHandler\RequestMapper;

use Illuminate\Database\Query\Builder;

class DriverSupportRepository
{
    /**
     * @var RequestMapper
     */
    private $requestMapper;

    /**
     * DriverSupportRepository constructor.
     * @param RequestMapper $requestMapper
     */
    public function __construct(RequestMapper $requestMapper)
    {
        $this->requestMapper = $requestMapper;
    }


    /**
     * Get a list of driver support reasons filtered by keyword and date range
     *      (NOTE: filters are sent in the request header)
     *
     * @return array|static[]
     */
    public function getList()
    {
        // set mappings to filters
        $this->requestMapper->setMappings([
            "driver_id" => [
                'field' => Driver::TABLE . '.driver_id'
            ],
            "driver_name" => [
                'field' => Driver::TABLE . '.known_name'
            ],
        ]);


        // driver support reasons for typical trips
        /* @var Builder $normalBuilder */
        $normalBuilder = app('db')->table(DriverSupportReasonLog::TABLE);

        $normalBuilder->addSelect([DriverSupportReasonLog::TABLE . '.id AS support_id',
                        DriverSupportReasonLog::TABLE . '.driver_id',
                        Driver::TABLE . '.known_name AS driver_name',
                        MotorModel::TABLE . '.model_name AS vehicle_type',
                        DriverContactInfo::TABLE . '.reachable_number AS driver_phone',
                        app('db')->raw("CONCAT(" . DriverSupportReasonLog::TABLE . ".support, ' - ', " . DriverSupportReasonLog::TABLE . ".reason) AS support_reason"),
                        PassengerLog::TABLE . '.travel_status AS trip_status',
                        PassengerLog::TABLE . '.current_location AS pickup_location',
                        PassengerLog::TABLE . '.pickup_latitude',
                        PassengerLog::TABLE . '.pickup_longitude',
                        PassengerLog::TABLE . '.drop_location',
                        PassengerLog::TABLE . '.drop_latitude',
                        PassengerLog::TABLE . '.drop_longitude',
                        DriverSupportReasonLog::TABLE . '.trip_id',
                        app('db')->raw("CONCAT(" . Passenger::TABLE . ".name, ' ', " . Passenger::TABLE . ".lastname) AS passenger_name"),
                        Passenger::TABLE . '.phone AS passenger_phone',
                        DriverSupportReasonLog::TABLE . '.status',
                        DriverSupportReasonLog::TABLE . '.created_at']);

        $normalBuilder->join(Driver::TABLE, DriverSupportReasonLog::TABLE . '.driver_id', '=', Driver::TABLE . '.driver_id')
                      ->join(PassengerLog::TABLE, DriverSupportReasonLog::TABLE . '.trip_id', '=', PassengerLog::TABLE . '.passengers_log_id')
                      ->join(Taxi::TABLE, PassengerLog::TABLE . '.taxi_id', '=', Taxi::TABLE . '.taxi_id')
                      ->join(MotorModel::TABLE, Taxi::TABLE . '.taxi_model', '=', MotorModel::TABLE . '.model_id')
                      ->join(Passenger::TABLE, PassengerLog::TABLE . '.passengers_id', '=', Passenger::TABLE . '.id')
                      ->join(DriverContactInfo::TABLE, Driver::TABLE . '.driver_id', '=', DriverContactInfo::TABLE . '.driver_id');

        $normalBuilder->where(DriverSupportReasonLog::TABLE . '.trip_id', '!=', DriverSupportReasonLog::NO_TRIP);

        // apply dynamic filters
        $this->requestMapper->applyFilters($normalBuilder);


        // driver support reasons for road pickup trips
        /* @var Builder $roadPickupBuilder */
        $roadPickupBuilder = app('db')->table(DriverSupportReasonLog::TABLE);

        $roadPickupBuilder->addSelect([DriverSupportReasonLog::TABLE . '.id AS support_id',
                            DriverSupportReasonLog::TABLE . '.driver_id',
                            Driver::TABLE . '.known_name AS driver_name',
                            MotorModel::TABLE . '.model_name AS vehicle_type',
                            DriverContactInfo::TABLE . '.reachable_number AS driver_phone',
                            app('db')->raw("CONCAT(" . DriverSupportReasonLog::TABLE . ".support, ' - ', " . DriverSupportReasonLog::TABLE . ".reason) AS support_reason"),
                            app('db')->raw("-1 AS trip_status"),
                            app('db')->raw("'' AS pickup_location"),
                            app('db')->raw("'' AS pickup_latitude"),
                            app('db')->raw("'' AS pickup_longitude"),
                            app('db')->raw("'' AS drop_location"),
                            app('db')->raw("'' AS drop_latitude"),
                            app('db')->raw("'' AS drop_longitude"),
                            DriverSupportReasonLog::TABLE . '.trip_id',
                            app('db')->raw("'' AS passenger_name"),
                            app('db')->raw("'' AS passenger_phone"),
                            DriverSupportReasonLog::TABLE . '.status',
                            DriverSupportReasonLog::TABLE . '.created_at']);

        $roadPickupBuilder->join(Driver::TABLE, DriverSupportReasonLog::TABLE . '.driver_id', '=', Driver::TABLE . '.driver_id')
                          ->join(TaxiDriverMap::TABLE, TaxiDriverMap::TABLE . '.mapping_driverid', '=', Driver::TABLE . '.driver_id')
                          ->join(Taxi::TABLE, TaxiDriverMap::TABLE . '.mapping_taxiid', '=', Taxi::TABLE . '.taxi_id')
                          ->join(MotorModel::TABLE, Taxi::TABLE . '.taxi_model', '=', MotorModel::TABLE . '.model_id')
                          ->join(DriverContactInfo::TABLE, Driver::TABLE . '.driver_id', '=', DriverContactInfo::TABLE . '.driver_id');

        $roadPickupBuilder->where(DriverSupportReasonLog::TABLE . '.trip_id', '=', DriverSupportReasonLog::NO_TRIP);

        // apply dynamic filters
        $this->requestMapper->applyFilters($roadPickupBuilder);

        
        // get all bindings to a single array to be used in the combined builder
        $normalBindings = $normalBuilder->getBindings();
        $roadPickupBindings = $roadPickupBuilder->getBindings();

        // append road pickup bindings to normal bindings
        //  (NOTE: the order of the resulting array is very important, so don't rearrange the array)
        foreach($roadPickupBindings as $element)
        {
            $normalBindings[] = $element;
        }

        // combine both queries
        /* @var Builder $combinedBuilder */
        $combinedBuilder = app('db')->table(app('db')->raw('(' . $normalBuilder->union($roadPickupBuilder)->toSql() .') AS driver_reason_log'));

        $combinedBuilder->orderBy('status', 'asc');
        $combinedBuilder->orderBy('created_at', 'desc');

        $combinedBuilder->setBindings($normalBindings);

        return $combinedBuilder->paginate();
    }


    /**
     * Update the driver support status
     *
     * @param $supportEntryId
     * @param $status
     * @return boolean
     */
    public function updateStatus($supportEntryId, $status)
    {
        /* @var DriverSupportReasonLog $supportEntry */
        $supportEntry = DriverSupportReasonLog::find($supportEntryId);

        $supportEntry->status = $status;

        return $supportEntry->save();
    }

}