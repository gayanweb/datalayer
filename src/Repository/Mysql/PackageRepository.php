<?php

namespace Pickme\DataAccess\Repository\Mysql;

use Pickme\DataAccess\Repository\Mysql\Model\ModifiedFare;
use Pickme\DataAccess\Repository\Mysql\Model\Package;

use Lib\RequestHandler\RequestMapper;

use Illuminate\Database\Query\Builder;

class PackageRepository
{
    /**
     * @var Package
     */
    private $package;

    /**
     * @var ModifiedFare
     */
    private $modifiedFare;

    /**
     * @var RequestMapper
     */
    private $requestMapper;


    /**
     * PackageRepository constructor.
     *
     * @param RequestMapper $requestMapper
     * @param Package $package
     * @param ModifiedFare $modifiedFare
     */
    public function __construct(RequestMapper $requestMapper,
                                Package $package,
                                ModifiedFare $modifiedFare)
    {
        $this->package = $package;
        $this->modifiedFare = $modifiedFare;
        $this->requestMapper = $requestMapper;
    }


    /**
     * Get a list of packages
     *
     * @return array|static[]
     */
    public function getList()
    {
        // set mappings to filters and fields __
        $this->requestMapper->setMappings([
            "package_vehicle" => [
                'field' => Package::TABLE . '.vehicle_type'
            ],
            "package_type" => [
                'field' => Package::TABLE . '.package_type'
            ],
            "package_id" => [
                'field' => Package::TABLE . '.id'
            ],
            "package_name" => [
                'field' => Package::TABLE . '.package_name'
            ],
        ]);


        /* @var Builder $builder */
        $builder = app('db')->table(Package::TABLE);

        // apply unique filters ___
        // get only active packages
        $builder->where(Package::TABLE . '.status', '=', Package::ACTIVE);

        // apply dynamic filters ___
        $this->requestMapper->applyFilters($builder);
        $this->requestMapper->applyFields($builder);


        return $builder->get();

    }


    /**
     * Get a package by a single field
     *
     * @param $field
     * @param $value
     * @param array $returnFields
     * @return mixed
     */
    public function getBy($field, $value, $returnFields = ['*'])
    {
        return $this->package->where($field, '=', $value)->first($returnFields);
    }


    /**
     * Create a package
     *
     * @param $package
     * @return static
     */
    public function create($package)
    {
        return $this->package->create($package);
    }


    /**
     * Create a modified package for a booking
     *
     * @param $modifiedPackage
     * @return static
     */
    public function createModified($modifiedPackage)
    {
        return $this->modifiedFare->create($modifiedPackage);
    }


    /**
     * Delete a modified package for a booking
     *
     * @param $field
     * @param $value
     * @return mixed
     */
    public function deleteModifiedBy($field, $value)
    {
        return $this->modifiedFare->where($field, '=', $value)->delete();
    }
}