<?php

namespace Pickme\DataAccess\Repository\Mysql;

use Pickme\DataAccess\Repository\Mysql\Model\Driver;
use Pickme\DataAccess\Repository\Mysql\Model\DriverContactInfo;
use Pickme\DataAccess\Repository\Mysql\Model\DriverRequestCurrent as DriverRequest;
use Pickme\DataAccess\Repository\Mysql\Model\MotorModel;
use Pickme\DataAccess\Repository\Mysql\Model\Passenger;
use Pickme\DataAccess\Repository\Mysql\Model\PassengerLogCurrent as PassengerLog;
use Pickme\DataAccess\Repository\Mysql\Model\PromoCode;
use Pickme\DataAccess\Repository\Mysql\Model\Taxi;
use Pickme\DataAccess\Repository\Mysql\Model\TaxiDriverMap;
use Pickme\DataAccess\Repository\Mysql\Model\Transaction;

use Lib\RequestHandler\RequestMapper;

use Carbon\Carbon;
use Illuminate\Database\Query\Builder;

class DriverSequenceRepository
{
    /**
     * @const How far back records are fetched from (in months)
     */
    const HISTORY = 50;

    /**
     * @var RequestMapper
     */
    private $requestMapper;

    /**
     * DriverSupportRepository constructor.
     *
     * @param RequestMapper $requestMapper
     */
    public function __construct(RequestMapper $requestMapper)
    {
        $this->requestMapper = $requestMapper;
    }


    /**
     * Get the list of driver sequence filtered by keyword and date range
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getList()
    {
        // set mappings to filters
        $this->requestMapper->setMappings([
            "driver_id" => [
                'field' => DriverRequest::TABLE . '.selected_driver'
            ],
            "driver_name" => [
                'field' => Driver::TABLE . '.known_name'
            ],
            "driver_phone" => [
                'field' => DriverContactInfo::TABLE . '.reachable_number'
            ],
            "passenger_phone" => [
                'field' => Passenger::TABLE . '.phone'
            ],
            "passenger_name" => [
                'field' => Passenger::TABLE . '.name'
            ],
            "trip_status" => [
                'field' => PassengerLog::TABLE . '.travel_status'
            ],
            "promo_code" => [
                'field' => PassengerLog::TABLE . '.promocode'
            ],
            "pickup_date" => [
                'field' => PassengerLog::TABLE . '.pickup_time',
                'callBack' => function($value) {
                                $date = Carbon::parse($value);
                                return $date->format('Y-m-d');
                            }
            ],
            "booked_from" => [
                'field' => PassengerLog::TABLE . '.booking_from'
            ],
        ]);


        // raw sub queries to be added to the sequence builder
        // (NOTE: raw sub queries are used to avoid complications while dealing with bindings of multiple dynamically built queries)
        $driverNameQuery = "SELECT " . Driver::TABLE . ".known_name FROM " . Driver::TABLE .
                            " WHERE " . Driver::TABLE . ".driver_id = ";

        $driverPhoneQuery = "SELECT " . DriverContactInfo::TABLE . ".reachable_number FROM " . DriverContactInfo::TABLE .
                            " WHERE " . DriverContactInfo::TABLE . ".driver_id = ";

        $taxiIdQuery = "SELECT " . TaxiDriverMap::TABLE . ".mapping_taxiid FROM " . TaxiDriverMap::TABLE .
                        " WHERE " . TaxiDriverMap::TABLE . ".mapping_status = 'A' AND " . TaxiDriverMap::TABLE . ".mapping_driverid = ";

        $taxiModelQuery = "SELECT " . MotorModel::TABLE . ".model_name FROM " . TaxiDriverMap::TABLE .
                            " LEFT JOIN " . Taxi::TABLE . " ON " . Taxi::TABLE . ".taxi_id = " . TaxiDriverMap::TABLE . ".mapping_taxiid" .
                            " LEFT JOIN " . MotorModel::TABLE . " ON " . MotorModel::TABLE . ".model_id = " . Taxi::TABLE . ".taxi_model" .
                            " WHERE " . TaxiDriverMap::TABLE . ".mapping_status = 'A' AND " . TaxiDriverMap::TABLE . ".mapping_driverid = ";

        $rejectedDriverIdQuery = "SUBSTRING_INDEX(" . DriverRequest::TABLE . ".rejected_timeout_drivers , ',', 1)";

        /* @var Builder $builder */
        $builder = app('db')->table(PassengerLog::TABLE);

        $builder->addSelect([
                        // trip details
                        PassengerLog::TABLE . '.passengers_log_id AS trip_id',
                        PassengerLog::TABLE . '.createdate',
                        PassengerLog::TABLE . '.travel_status',
                        PassengerLog::TABLE . '.pickup_time',
                        PassengerLog::TABLE . '.dispatch_time',
                        PassengerLog::TABLE . '.drop_time',

                        // locations
                        PassengerLog::TABLE . '.current_location',
                        PassengerLog::TABLE . '.pickup_latitude',
                        PassengerLog::TABLE . '.pickup_longitude',
                        PassengerLog::TABLE . '.drop_location',
                        PassengerLog::TABLE . '.drop_latitude',
                        PassengerLog::TABLE . '.drop_longitude',

                        // promo
                        PassengerLog::TABLE . '.promocode',
                        PromoCode::TABLE . '.discount_type',
                        PromoCode::TABLE . '.promo_discount',

                        // costing
                        PassengerLog::TABLE . '.payment_method',
                        Transaction::TABLE . '.distance',
                        Transaction::TABLE . '.fare',
                        Transaction::TABLE . '.passenger_discount',
                        Transaction::TABLE . '.amt AS total_fare',

                        // passenger
                        PassengerLog::TABLE . '.passengers_id',
                        Passenger::TABLE . '.name AS passenger_name',
                        Passenger::TABLE . '.phone AS passenger_phone',

                        // accepted driver
                        DriverRequest::TABLE . '.selected_driver',
                        Driver::TABLE . '.known_name AS selected_driver_name',
                        DriverContactInfo::TABLE . '.reachable_number AS selected_driver_phone',
                        app('db')->raw("($taxiIdQuery" . DriverRequest::TABLE . ".selected_driver) AS selected_taxi_id"),
                        app('db')->raw("($taxiModelQuery" . DriverRequest::TABLE . ".selected_driver) AS selected_model_name"),

                        // rejected driver (NOTE: spaces between variables are very important)
                        app('db')->raw("$rejectedDriverIdQuery AS rejected_timeout_driver"),
                        app('db')->raw("($driverNameQuery $rejectedDriverIdQuery) AS rejected_driver_name"),
                        app('db')->raw("($driverPhoneQuery $rejectedDriverIdQuery) AS rejected_driver_phone"),
                        app('db')->raw("($taxiIdQuery $rejectedDriverIdQuery) AS rejected_taxi_id"),
                        app('db')->raw("($taxiModelQuery $rejectedDriverIdQuery) AS rejected_model_name"),
                        DriverRequest::TABLE . '.rejected_timeout_drivers',

                        // company
                        PassengerLog::TABLE . '.company_id',

                        // feedback
                        PassengerLog::TABLE . '.comments',
                        PassengerLog::TABLE . '.driver_comments',
                        PassengerLog::TABLE . '.rating',
                        PassengerLog::TABLE . '.sos_sent',
                        PassengerLog::TABLE . '.driver_sos_sent',
        ]);

        $builder->join(Passenger::TABLE, Passenger::TABLE . '.id', '=', PassengerLog::TABLE . '.passengers_id')
                ->leftJoin(DriverRequest::TABLE, DriverRequest::TABLE . '.trip_id', '=', PassengerLog::TABLE . '.passengers_log_id')
                ->leftJoin(Driver::TABLE, Driver::TABLE . '.driver_id', '=', DriverRequest::TABLE . '.selected_driver')
                ->leftJoin(DriverContactInfo::TABLE, DriverContactInfo::TABLE . '.driver_id', '=', DriverRequest::TABLE . '.selected_driver')
                ->leftJoin(Transaction::TABLE, Transaction::TABLE . '.passengers_log_id', '=', PassengerLog::TABLE . '.passengers_log_id')
                ->leftJoin(PromoCode::TABLE, PromoCode::TABLE . '.promocode', '=', PassengerLog::TABLE . '.promocode');


        $builder->where(PassengerLog::TABLE . '.createdate', '>', app('db')->raw("NOW() - INTERVAL " . self::HISTORY . " MONTH"));
//        $builder->where(PassengerLog::TABLE . '.created_date_date', '>', '');

        // apply dynamic filters
        $this->requestMapper->applyFilters($builder);

        $builder->orderBy(PassengerLog::TABLE . '.passengers_log_id', 'desc');

        //echo $builder->toSql(); return \Response::json(1);

//        // use caching (NOTE: 10 seconds ~= 0.17 minutes)
//        $result = app('cache')->remember('driver_sequence', 0.17, function() use ($builder)
//        {
//            return $builder->paginate();
//        });
//
//        return $result;

        return $builder->simplePaginate($this->requestMapper->getPaging()['perPage'])->items();
    }

}