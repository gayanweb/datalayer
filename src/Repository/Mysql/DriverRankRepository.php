<?php
/**
 * Created by PhpStorm.
 * User: user1
 * Date: 8/31/2016
 * Time: 10:57 AM
 */

namespace Pickme\DataAccess\Repository\Mysql;


use Pickme\DataAccess\Repository\Mysql\Model\DriverRank;

class DriverRankRepository
{
    /**
     * @var DriverRank
     */
    private $driverRank;


    /**
     * DriverRankRepository constructor.
     * @param DriverRank $driverRank
     */
    public function __construct(DriverRank $driverRank)
    {
        $this->driverRank = $driverRank;
    }

    public function store($rank)
    {
        $rankInfo = [
            'driver_id' => $rank['driver_id'],
            'rank' => $rank['rank']
        ];

        return $this->driverRank->forceCreate($rankInfo)->toArray();
    }
}