<?php

namespace Pickme\DataAccess\Repository\Mysql\Model;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    const TABLE = 'driver_information';

    protected $table = 'driver_information';

    protected $primaryKey = 'driver_id';

    public $timestamps = false;

}