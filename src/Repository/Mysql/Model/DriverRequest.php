<?php

namespace Pickme\DataAccess\Repository\Mysql\Model;

use Illuminate\Database\Eloquent\Model;

class DriverRequest extends Model
{
    const TABLE = 'driver_request_details';

    protected $table = 'driver_request_details';

    protected $primaryKey = 'request_id';

    public $timestamps = false;

}