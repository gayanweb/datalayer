<?php

namespace Pickme\DataAccess\Repository\Mysql\Model;

use Illuminate\Database\Eloquent\Model;

class Passenger extends Model
{
    const TABLE = 'passengers';

    const ACTIVE = 'A';
    const INACTIVE = 'D';

    const CREATED_BY_PASSENGER = 1;
    const CREATED_BY_ADMIN = 2;
    const CREATED_BY_COMPANY = 3;

    const ACTIVATED = 1;

    protected $table = 'passengers';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'phone',
        'name',
        'email',
        'created_date',
        'activation_status',
        'user_status',
        'created_by',
        'passenger_cid',
        'country_id'
    ];

}