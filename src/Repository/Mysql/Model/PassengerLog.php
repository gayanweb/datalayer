<?php

namespace Pickme\DataAccess\Repository\Mysql\Model;

use Illuminate\Database\Eloquent\Model;

class PassengerLog extends Model
{
    const TABLE = 'passengers_log';

    const BOOK_BY_PASSENGER = 1;
    const BOOK_BY_OPERATOR = 2;

    const BOOK_NOW = 0;
    const BOOK_LATER = 1;

    const BOOK_FROM_WEB = 0;
    const BOOK_FROM_DEVICE = 1;
    const BOOK_FROM_STREET = 2;

    const TRIP_STATUS_NOT_COMPLETE = 0;
    const TRIP_STATUS_COMPLETE = 1;
    const TRIP_STATUS_IN_PROGRESS = 2;
    const TRIP_STATUS_START_PICKUP = 3;
    const TRIP_STATUS_PASSENGER_CANCEL = 4;
    const TRIP_STATUS_WAITING_PAYMENT = 5;
    const TRIP_STATUS_MISSED = 6;
    const TRIP_STATUS_DISPATCHED = 7;
    const TRIP_STATUS_DISPATCHER_CANCEL = 8;
    const TRIP_STATUS_CONFIRMED = 9;

    const COMPANY_NONE = 0;

    protected $table = 'passengers_log';

    protected $primaryKey = 'passengers_log_id';

    public $timestamps = false;

    protected $fillable = [
        'passengers_id',
        'passenger_name',
        'current_location',
        'pickup_latitude',
        'pickup_longitude',
        'drop_location',
        'drop_latitude',
        'drop_longitude',
        'pickup_time',
        'promocode',
        'taxi_modelid',
        'company_id',
        'travel_status',
        'booking_from',
        'notes_driver',
        'bookingtype',
        'bookby',
    ];
}