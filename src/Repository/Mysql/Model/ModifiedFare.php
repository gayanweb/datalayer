<?php

namespace Pickme\DataAccess\Repository\Mysql\Model;

use Illuminate\Database\Eloquent\Model;

class ModifiedFare extends Model
{
    const TABLE = 'company_modified_fare';

    const BOOK_FROM_DISPATCHER = 0;
    const BOOK_FROM_PASSENGER = 1;

    protected $table = 'company_modified_fare';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'trip_id',
        'passenger_id',
        'company_id',
        'min_km',
        'min_fare',
        'above_km',
        'waiting_time',
        'night_fare',
        'promo_code',
        'free_waiting_time',
        'ride_hours',
        'extra_ride_fare',
        'model_id',
        'booking_from',
        'package_id',
        'package_type',
        'driver_bata',
    ];

}