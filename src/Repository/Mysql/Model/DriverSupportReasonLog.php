<?php

namespace Pickme\DataAccess\Repository\Mysql\Model;

use Illuminate\Database\Eloquent\Model;

class DriverSupportReasonLog extends Model
{
    const TABLE = 'driver_support_reason_log';

    const ACTIVE = 1;
    const INACTIVE = 0;

    const NO_TRIP = 0; // driver is not in a trip

    protected $table = 'driver_support_reason_log';

    protected $primaryKey = 'id';

    public $timestamps = false;

}