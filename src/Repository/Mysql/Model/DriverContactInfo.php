<?php

namespace Pickme\DataAccess\Repository\Mysql\Model;

use Illuminate\Database\Eloquent\Model;

class DriverContactInfo extends Model
{
    const TABLE = 'driver_contact_information';

    protected $table = 'driver_contact_information';

    protected $primaryKey = 'contact_information_id';

    public $timestamps = false;

}