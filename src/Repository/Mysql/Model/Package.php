<?php

namespace Pickme\DataAccess\Repository\Mysql\Model;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    const TABLE = 'flat_rate_packages';

    const ACTIVE = 1;
    const INACTIVE = 0;

    const TYPE_HOUR = 0;
    const TYPE_DAY = 1;

    const TRIP_TYPE_ONE_WAY = 0;
    const TRIP_TYPE_RETURN = 1;

    const MODULE_ALL = 0;
    const MODULE_DISPATCHER = 1;
    const MODULE_PASSENGER = 2;

    const ACTIVATED = 1;

    protected $table = 'flat_rate_packages';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'vehicle_type',
        'package_type',
        'package_name',
        'min_km',
        'min_fare',
        'additional_km_fare',
        'waiting_time_fare',
        'free_waiting_time',
        'night_fare',
        'ride_hours',
        'extra_ride_fare',
        'driver_bata',
        'status',
    ];

}