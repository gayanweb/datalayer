<?php

namespace Pickme\DataAccess\Repository\Mysql\Model;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    const TABLE = 'transacation';

    protected $table = 'transacation';

    protected $primaryKey = 'id';

    public $timestamps = false;

}