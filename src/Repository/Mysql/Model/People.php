<?php

namespace Pickme\DataAccess\Repository\Mysql\Model;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    const TABLE = 'people';

    protected $table = 'people';

    protected $primaryKey = 'id';

    public $timestamps = false;

}