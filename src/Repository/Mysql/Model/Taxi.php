<?php

namespace Pickme\DataAccess\Repository\Mysql\Model;

use Illuminate\Database\Eloquent\Model;

class Taxi extends Model
{
    const TABLE = 'taxi';

    protected $table = 'taxi';

    protected $primaryKey = 'taxi_id';

    public $timestamps = false;

}