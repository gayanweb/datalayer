<?php

namespace Pickme\DataAccess\Repository\Mysql\Model;

use Illuminate\Database\Eloquent\Model;

class MotorModel extends Model
{
    const TABLE = 'motor_model';

    const ACTIVE = 'A';
    const INACTIVE = 'D';

    protected $table = 'motor_model';

    protected $primaryKey = 'model_id';

    public $timestamps = false;

}