<?php

namespace Pickme\DataAccess\Repository\Mysql\Model;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    const TABLE = 'company';

    const ACTIVE = 'A';
    const INACTIVE = 'D';

    protected $table = 'company';

    protected $primaryKey = 'cid';

    public $timestamps = false;

}