<?php

namespace Pickme\DataAccess\Repository\Mysql\Model;

use Illuminate\Database\Eloquent\Model;

class DriverRank extends Model
{
    const TABLE = 'driver_ranking';

    protected $table = 'driver_ranking';

    protected $primaryKey = 'id';

}