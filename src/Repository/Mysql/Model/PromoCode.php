<?php

namespace Pickme\DataAccess\Repository\Mysql\Model;

use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model
{
    const TABLE = 'passengers_promo';

    const PASSENGER = '1';

    const DRIVER = '1';
    const DEPARTMENT = '3';
    const GENERAL = '6';

    protected $table = 'passengers_promo';

    protected $primaryKey = 'passenger_promoid';

    public $timestamps = false;

}