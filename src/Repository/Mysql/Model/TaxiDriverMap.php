<?php

namespace Pickme\DataAccess\Repository\Mysql\Model;

use Illuminate\Database\Eloquent\Model;

class TaxiDriverMap extends Model
{
    const TABLE = 'taxi_driver_mapping';

    protected $table = 'taxi_driver_mapping';

    protected $primaryKey = 'mapping_id';

    public $timestamps = false;

}