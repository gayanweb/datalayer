<?php

namespace Pickme\DataAccess\Repository\Mysql;

use Pickme\DataAccess\Repository\Mysql\Model\PromoCode;

class PromotionRepository
{
    /**
     * @var PromoCode
     */
    private $promoCode;


    /**
     * PromoCodeRepository constructor.
     *
     * @param PromoCode $promoCode
     */
    public function __construct(PromoCode $promoCode)
    {
        $this->promoCode = $promoCode;
    }


    /**
     * Get details of the corporate promo code.
     *
     * @param $code
     * @return mixed
     */
    public function getPromoCodeDetails($code)
    {
        $code = (string)$code;

        return app('db')->table(PromoCode::TABLE)
                            ->select('corporate_company_id', 'corporate_dep_id', 'passenger_id', 'promo_type', 'promocode', 'discount_type',
                                     'promo_discount', 'promo_used', 'start_date', 'expire_date', 'promo_limit')
                            ->where('promocode', '=', $code)
                            ->where('passenger_id', '=', '0')
                            ->first();
    }

    /**
     * Get a promotion by a single field
     *
     * @param $field
     * @param $value
     * @param array $returnFields
     * @return mixed
     */
    public function getBy($field, $value, $returnFields = ['*'])
    {
        return $this->promoCode->where($field, '=', $value)->first($returnFields);
    }
}