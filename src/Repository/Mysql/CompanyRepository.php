<?php

namespace Pickme\DataAccess\Repository\Mysql;

use Pickme\DataAccess\Repository\Mysql\Model\Company;

use Lib\RequestHandler\RequestMapper;

use Illuminate\Database\Query\Builder;

class CompanyRepository
{
    /**
     * @var Company
     */
    private $company;

    /**
     * @var RequestMapper
     */
    private $requestMapper;


    /**
     * PassengerRepository constructor.
     *
     * @param RequestMapper $requestMapper
     * @param Company $company
     */
    public function __construct(RequestMapper $requestMapper,
                                Company $company)
    {
        $this->company = $company;
        $this->requestMapper = $requestMapper;
    }


    /**
     * Get a list of companies
     *
     * @return array|static[]
     */
    public function getList()
    {
        // set mappings to filters and fields ___
        $this->requestMapper->setMappings([
            "company_id" => [
                'field' => Company::TABLE . '.cid'
            ],
            "company_name" => [
                'field' => Company::TABLE . '.company_name'
            ],
        ]);


        /* @var Builder $builder */
        $builder = app('db')->table(Company::TABLE);

        // apply unique filters ___
        // get only active companies
        $builder->where(Company::TABLE . '.company_status', '=', Company::ACTIVE);

        // apply dynamic filters ___
        $this->requestMapper->applyFilters($builder);
        $this->requestMapper->applyFields($builder);

        // limit ___
        $builder->limit(15);


        return $builder->get();

    }

}