<?php

namespace Pickme\DataAccess\Repository\Mysql;

use Pickme\DataAccess\Repository\Mysql\Model\MotorModel;

use Lib\RequestHandler\RequestMapper;

use Illuminate\Database\Query\Builder;

class VehicleRepository
{
    /**
     * @var RequestMapper
     */
    private $requestMapper;


    /**
     * VehicleRepository constructor.
     *
     * @param RequestMapper $requestMapper
     */
    public function __construct(RequestMapper $requestMapper)
    {
        $this->requestMapper = $requestMapper;
    }


    /**
     * Get the list of vehicle types
     *
     * @return mixed
     */
    public function getTypes()
    {
        $fields = [
            'model_id',
            'model_name',
        ];

        /* @var Builder $builder */
        $builder = app('db')->table(MotorModel::TABLE);

        // apply unique filters ___
        // get only active vehicle models
        $builder->where(MotorModel::TABLE . '.model_status', '=', MotorModel::ACTIVE);

        return $builder->get($fields);
    }

}